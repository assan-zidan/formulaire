<!DOCTYPE html>
<html>
<head>
	<title>Incription</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/Bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="container-fluid fond">
		<div class="row">
			<div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8 forme">
				<div class="row">
					<h2>inscription</h2>
				</div>
				<div class="row forme1">
					<form method="post" action="pages/traitement.php" enctype="multipart/form-data">
						<div class="form-group">	
							<label>Nom</label>
							<div class="input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-user colr"></span>
								</span>		
								<input class="form-control" id="nom" type="text" name="nom" pattern="^[A-Za-z0-9_.]+${2,20}" placeholder="veillez entrer votre nom" required="">
								<!-- <span class="input-group-addon">
									<span class="validity"></span>
								</span> -->
							</div>	
							<p><span>le nom ne doit pas contenir les ponctuations et avoir un seul caractere</span></p>

							
						</div>
						<div class="form-group">
							<label>Prenom</label>
							<div class="input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-user colr"></span>
								</span>	
								<input id="prenom" class="form-control" type="text" name="prenom" pattern="^[A-Za-z0-9_.]+${2,20}" placeholder="Veillez entrer votre prenom" required="">
								<!-- <span class="input-group-addon">
									<span class="validity"></span>
								</span> -->
							</div>
							<p><span>le prenom ne doit pas contenir les ponctuations et avoir un seul caractere</span></p>
						</div>
					    <div class="form-group">
							<label>Email</label>
							<div class="input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-envelope colr"></span>
								</span>	
								<input type="email" id="email" name="email" class="form-control" placeholder="myname@example.com" required="">
								<!-- <span class="input-group-addon">
									<span class="validity"></span>
								</span> -->
							</div>
							<p><span>l'email doit contenir le caractere @</span></p>
						</div>
						<div class="form-group">
							<label>Date de naissance</label>
							<div class="input-group">	
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar colr"></span>
								</span>	
								<input class="form-control" type="date"  name="date_naissance" required="">
							</div>
						</div>
						<div class="form-group">
							<label>Sexe</label>
							<div class="input-group">
								<span class="input-group-addon">
									<span class="fa fa-transgender colr"></span>
								</span>	
								<select name="sexe" class="form-control" required="">
									<option value="Homme">Homme</option>
									<option>Femme</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Choix du pays</label>
							<div class="input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-flag colr"></span>
								</span>	
								<select name="choix_pays" class="form-control" required="">
									<optgroup label="Afrique">
										<option>Cameroun</option>
										<option>Cote d'Ivoire</option>
										<option>Senegale</option>
										<option>Mali</option>
										<option>Togo</option>
										<option>Gabon</option>
										<option>Maroc</option>
										<option>Tunisie</option>
										<option>Algerie</option>
									</optgroup>
										<optgroup label="Europe">
										<option>France</option>
										<option>Italie</option>
										<option>Espagne</option>
										<option>Belgique</option>
									    <option>Allemagne</option>
										<option>Portugale</option>
										<option>Suisse</option>
										</optgroup>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Numero de telephone</label>
							<div class="input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-earphone colr"></span>
								</span>	
								<input type="tel" name="phone" minlength="8" maxlength="20" required="" class="form-control">
								<!-- <span class="input-group-addon">
									<span class="validity"></span>
								</span> -->
							</div>
						</div>
						<div class="form-group">
							<label>Mot de passe</label>
							<div class="input-group">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-lock colr"></span>
								</span>	
								<input type="password" name="passeword" class="form-control" minlength="8" required="">
								<!-- <span class="input-group-addon">
									<span class="validity"></span>
								</span> -->
							</div>
						</div>
						<div class="form-group">
							<label>Photo de profil</label>
							<div class="input-group">	
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-picture colr"></span>
								</span>	
								<input type="file" name="photo_profil" class="form-control" required="" accept="image/*" onchange="loadFile(event)">

							</div>
							<div class="row" id="visua1">
								<img id="affichage" class="visua"></img>
							</div>
						</div>
				<!-- affichage photo de profil -->
						
						<div class="form-group">
							<button type="submit" class="btn pull pull-right hov"><span>Enregistrer</span></button>
							<button type="reset" class="btn  hov1"><span>Renitialiser</span></button>
						
						</div>
					</form>
				
			</div>
		</div>
	</div>








	<script type="text/javascript" src="javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		var loadFile = function(event) {
        var profil = document.getElementById('affichage');
        var affichdiv=document.getElementById('visua1');
        affichdiv.style.display="block";
        profil.src = URL.createObjectURL(event.target.files[0]);
      };



      var p_content=document.getElementsByTagName('p');
		// console.log(p_content.length);

		for (var i = 0; i < p_content.length; i++) {
			p_content[i].style.display='none';
			console.log(p_content[i]);
		}


		// traitement sur le nom
		function nomtraitement() {
			var nom=document.getElementById('nom');
			nom.onchange=function(){
				if (nom.value.length<2) {
					p_content[0].style.display='block';
					p_content[0].style.color='red';
					return 0;
				}else{
					p_content[0].style.display='none';
					return 1;
				}
			}
		}


		// traitement sur le prenom
		function prenomtraitement() {
			var prenom=document.getElementById('prenom');
			prenom.onchange=function() {
				if (prenom.value.length<2) {
					p_content[1].style.display='block';
					p_content[1].style.color='red';
					return 0;
				}else{
					p_content[0].style.display='none';
					return 1;
				}
			}
		}


		// traitement sur l'email
		function emailtraitement() {
			var val_verif, taille, email=document.getElementById('email');
			email.onchange=function() {
				taille=email.value.length;
				for (var i = 0; i < taille; i++) {
					if (email.value[i]=='@') {
						val_verif=0;
						break;
					}else{
						val_verif=1;
					}
				}

				if (val_verif==0) {
					p_content[2].style.display='none';
					return 1;
				} else {
					p_content[2].style.display='block';
					p_content[2].style.color='red';
					return 0;
				}
			}
		}


		// appel des fonctions 
		nomtraitement();
		prenomtraitement();
		emailtraitement();
	</script>
</body>
</html>